# graphcoloring

This is a clone of repository graphcoloring from brrcrites repository.

`graphcoloring` is a collection of graph coloring algorithms for
coloring vertices of a graph such that no two adjacent vertices share
the same color. The algorithms are included via the embedded
‘GraphColoring’ C++ library,
<https://github.com/brrcrites/GraphColoring>.

## Installation

``` r
devtools::install_bitbucket("qzhudfci/graphcoloring")
```

## Example

`color_*` functions operate under `tidygraph` family and can be used to
color nodes within `mutate` context similar to `group_*` functions in
`tidygraph`.

``` r
library(graphcoloring)
library(tidygraph)
library(ggraph)

set.seed(42)

play_islands(5, 10, 0.8, 3) %>%
  mutate(color = as.factor(color_dsatur())) %>%
  ggraph(., layout = 'kk') +
  geom_edge_link(aes(alpha = ..index..), show.legend = FALSE) +
  geom_node_point(aes(color = color), size = 7) +
  theme_graph()
```

